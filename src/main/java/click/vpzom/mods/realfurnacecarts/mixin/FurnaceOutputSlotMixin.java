package click.vpzom.mods.realfurnacecarts.mixin;

import click.vpzom.mods.realfurnacecarts.IDropExperience;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.FurnaceOutputSlot;
import net.minecraft.screen.slot.Slot;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(FurnaceOutputSlot.class)
public class FurnaceOutputSlotMixin extends Slot {
    public FurnaceOutputSlotMixin(Inventory inventory, int index, int x, int y) {
        super(inventory, index, x, y);
    }

    @Shadow
    @Final
    private PlayerEntity player;

    @Inject(at = @At("RETURN"), method = "onCrafted(Lnet/minecraft/item/ItemStack;)V")
    private void onOnCrafted(ItemStack stack, CallbackInfo ci) {
        System.out.println("onOnCrafted");
        if(!player.world.isClient && inventory instanceof IDropExperience) {
            System.out.println("is IDropExperience");
            ((IDropExperience) inventory).dropExperience(player);
        }
    }
}
