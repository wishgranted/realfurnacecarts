package click.vpzom.mods.realfurnacecarts;

import net.minecraft.entity.player.PlayerEntity;

public interface IDropExperience {
    void dropExperience(PlayerEntity player);
}
